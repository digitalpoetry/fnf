
Framework in a File!
===

A micro framework in a single file featuring:
- View and post routing.
- Request validation.
- Customizable theme templates built with Bootstrap.
- Flexible Views.
- A loader class and a global object to access everything.
- Modules that quickly extend the application.
- Automatic asset embeding.
- A build system that compiles the script on the fly.

Working in a single file presents it's own brand of nuances:
- Images cannot not be including by conventional means.
- Code cannot be organized into files with namespaces.
- MVC structure is not an option.
- HTML, CSS and JS cannot be easily seperated from PHP and is difficult to work
  with.

FnF helps to overcome these challenges for rapid development of a single file
web application.


Project Notes
---

This project is a work in progess. As such, documentation may be incomplete.

### Installation

This project requires [GIT version control](https://git-scm.com/) and
[php composer](https://getcomposer.org/). Afetr GIT and Composer are installed,
open a terminal or windows command prompt.

Run the following command:
```bash
php composer install -d path/to/project
php path/to/project/build/install_project.php
```

### Coding Standards

Code MUST adhere to:
- Basic Coding Standard [PSR-1](http://www.php-fig.org/psr/psr-1/).
- Coding Style Guide [PSR-2](http://www.php-fig.org/psr/psr-2/).
- PHPDoc Standard [PSR-5](https://github.com/phpDocumentor/fig-standards/blob/master/proposed/phpdoc.md).
- Extended Coding Style Guide [PSR-12](https://github.com/php-fig/fig-standards/blob/master/proposed/extended-coding-style-guide.md).

### Getting started

To get started developing with FnF, download the source code and checkout the
files in the 'sources' directory. All of the code has documented inline. In
addition, there are 2 example Modules, a skeleton and a complete example.
Modules are how the application is extened. Custom or third party libraries may
also included as well.

#### File Structure

FnF has a HMVC or Hierarchical Model View Controller file structure. Modules can
mock the application structure recursively. The directory paths are completely
customizable.

To change the project structure:
- Edit build/config_structure.php
- Browse to inth build script in your browser, http://localhost/build/build.php?install=1
- Copy the files form the old directories to the new ones.

**NOTE:** All file names and class names must be unique!

By convention, filenames of files containing a class should match the class
name. Files should contain only one class per file.

Namespaces are not allowed to maintain php compatability and because there
could be only a single namespace in the compiled application anyways. Instead,
use the @package tag in your DocBlocks. This will group your code in the
generated api docs.

#### Static assets

At build time, FnF encodes static assets and injects them into your source code.
Assets are 'embeded' in the script for 2 reasons:
- Working with css, html and js in php is disorganized and difficult.
- Images must be embeded to keep everything 'in a file'.

By embedding assets at build time, assets can be worked with as individual files
to make life easier. Image assets are base64 encoded and embeded into the other
static assets and the php source code before it is written to file. Then the
other assets are fetched via http and embeded. This way static assets like css,
html and js files can be generated dynamically before the are encoded and
embeded into the source code.

#### Build Process

The build system allows editing of individual files and raw unencoded assets.
The overview is that the source php files are cantonated together into a single
file. All files in the assets directory are base64 encoded and embeded into the
source code before the compiled file is written.

Using the 'build.php' file, these assets are compiled or encoded on the fly,
then written to 'app.php' by default.

Using the 'build_run.php' file, the application is built and then parsed and
outputted to the page.

##### Caching

During the build process both precompiled and compiled versions of your files
are saved to the cache directory. This is done since it is resource costly to
base64 encode many images and files every time an edit is made or the page is
loaded. The source code can then be built from cache to speed to build times.
When building the script using caching, assets do not need to be re-encoded and
embeded every time.

If building source from cache is enabled, a default list of files will be built
from cache if cahe files are available. You can also specify which files to
build from cache.

If building assets from cache is enabled, all assets are embeded from encoded
cache if files are available.

The source code is compiled as follows:
1. The code is cantonated from the source files. By default, files are
   cantonated in the following order:
   - The header.php file.
   - Core files, in a specific order.
   - Files in the helpers directory, alphabetically by filename.
   - Files in the modules directory, alphabetically by filename.
   - Files in the utilities directory, alphabetically by filename.
   - The footer.php file.
2. Images are embeded into the source code and other asset files. By default,
   images are embeded in the following order:
   - Files in the assets 'css' directory.
   - Files in the assets 'html' directory.
   - Files in the assets 'js' directory.
   - The cantonated source code.
3. Remaining assets are embeded into the source code.
4. Before each asset is been embeded, it's static content is written to a file
   in the cache directory with the same filename. The static asset is then
   base64 encoded and written again to the cache directory with '.txt' appended
   to the filename.
5. The compiled source code is written to file.

To have your Module or Utility class included, simply put the file in the
modules or utilities directory. If you want to add a core file, add it to the
array in the build.php file.


Release Notes
---

No releases yet. Coming soon.
