<?php
/**
 * Framework in a File.
 *
 * The micro framework in a single file. Quickly develop and deploy scripts and
 * applications.
 *
 * @package    DigitalPoetry\FnF\Application\Qux\Quux
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */


/**
 * Quux Module Class Example.
 *
 * Description of your module, what it does and how to use it, goes here...
 * The Quux module example uses the Baz Utility example. Modules must extend the
 * Controller base class or the module will not be loaded.
 *
 * @package  DigitalPoetry\FnF\Quux\Controller
 * @author   Your Name <you@example.com>
 * @since    0.1.0 Basic Things
 */
class Quux extends Controller
{
	/**
	 * The page title tag.
	 *
	 * @var string
	 */
	public $title = 'Quux Module';

	/**
	 * The page header.
	 *
	 * @var string
	 */
	public $header = 'Quux Module Skeleton Example';

	/**
	 * The default method to run if not supplied as a query parameter.
	 *
	 * @var   string
	 */
	public $default_method = 'quux';

	/**
	 * Registers the module. Loads dependencies.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// Add items to menu.
		$this->add_nav_item(21, 'Quux');
	}

	/**
	 * Runs the module. Displays the output.
	 *
	 * @param array $post The http post if one was received.
	 * @return void
	 */
	public function quux($post = null)
	{
		/**
		 * Bring the $app global object into scope.
		 *
		 * @global Application.
		 */
		global $app;

		// Output.
		$output = '<p>This is the Quux module. The Quux module is a skeleton example, ' .
		'incase you would rather get started with that. Check out the source ' .
		'code <a href="https://gitlab.com/jlareaux/fnf" ' .
		'title="Framework in a File repository">here</a>.</p>';

		// Page Content.
		$app->output->set_content($output);

		// Render page.
		$app->output->render();
	}

} // Quux
