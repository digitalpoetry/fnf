<?php
/**
 * Framework in a File.
 *
 * The micro framework in a single file. Quickly develop and deploy scripts and
 * applications.
 *
 * @package    DigitalPoetry\FnF\Application\Bar
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */


/**
 * Bar Module Class Example.
 *
 * Description of your module, what it does and how to use it, goes here...
 * The Bar module example uses the Baz Utility example. modules must extend the
 * Controller base class or the module will not be loaded.
 *
 * @package  DigitalPoetry\FnF\Bar\Controller
 * @author   Your Name <you@example.com>
 * @since    0.1.0 Basic Things
 */
class Bar extends Controller
{
	/**
	 * The page title tag.
	 *
	 * @var string
	 */
	public $title = 'Bar Module';

	/**
	 * The page header.
	 *
	 * @var string
	 */
	public $header = 'Bar Module Skeleton Example';

	/**
	 * Registers the module. Loads dependencies.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// Add nav items to menu.
		$this->add_nav_item(11, 'Bar', $this->getModuleURL());
	}

	/**
	 * Runs the module. Displays the output.
	 *
	 * @param array $post The http post if one was received.
	 * @return void
	 */
	public function index($post = null)
	{
		/**
		 * Bring the $app global object into scope.
		 *
		 * @global Application.
		 */
		global $app;

		// Output.
		$output = '<p>This is the Bar module. The Bar module is a skeleton example, ' .
		'incase you would rather get started with that. Check out the source ' .
		'code <a href="https://gitlab.com/jlareaux/fnf" ' .
		'title="Framework in a File repository">here</a>.</p>';

		// More Output.
		$output .= '<hr>' . base64_decode('##BAR_HTML_ELEMENTS.HTML##');

		// Page Content.
		$app->output->set_content($output);

		// Render page.
		$app->output->render();
	}

} // Bar
