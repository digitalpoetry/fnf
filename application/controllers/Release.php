<?php
/**
 * Framework in a File.
 *
 * The micro framework in a single file. Quickly develop and deploy scripts and
 * applications.
 *
 * @package    DigitalPoetry\FnF\Application\Release
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */


/**
 * Release Module Class Example.
 *
 * This module will build a release of the application.
 *
 * @package  DigitalPoetry\FnF\Application\Release
 * @author   Your Name <you@example.com>
 * @since    0.1.0 Basic Things
 */
class Release extends Controller
{
	/**
	 * The page title tag.
	 *
	 * @var string
	 */
	public $title = 'Build a Release';

	/**
	 * The page header.
	 *
	 * @var string
	 */
	public $header = 'Build a Release';


	/**
	 * Registers the module. Loads dependencies.
	 *
	 * @return void
	 */
	public function __construct()
	{
		// Add nav items to menu.
		$this->add_nav_item(40, 'Build a Release', "#", array(
			array('href' => $this->getModuleURL() . '&release=app', 'title' => 'Application'),
			array('href' => $this->getModuleURL() . '&release=fnf', 'title' => 'File in a Framework'),
		));
	}

	/**
	 * Runs the module. Displays the output.
	 *
	 * @param array $post The http post if one was received.
	 * @return void
	 */
	public function index($post = null)
	{
		/**
		 * Bring the $app global object into scope.
		 *
		 * @global Application.
		 */
		global $app;

		// Output.
		$output = '<p>This is the Release module.</p>';

		// More Output.
		# $output .= '<hr>' . base64_decode('##RELEASE_BUILD_BUTTONS.HTML##');

		// Page Content.
		$app->output->set_content($output);

		// Render page.
		$app->output->render();
	}

} // Release
