<?php
/**
 * Framework in a File.
 *
 * The micro framework in a single file. Quickly develop and deploy scripts and
 * applications.
 *
 * @package    DigitalPoetry\FnF\Application\Foo
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */


/**
 * Foo Module Class Example.
 *
 * Description of your module, what it does and how to use it, goes here...
 * The Foo module example uses the Baz Utility example. Modules must extend the
 * Controller base class or the module will not be loaded.
 *
 * @package  DigitalPoetry\FnF\Foo\Controller
 * @author   Your Name <you@example.com>
 * @since    0.1.0 Basic Things
 */
class Foo extends Controller
{
	/**
	 * The page title tag.
	 *
	 * You can set the page title tag statically here, or dynamically in the run
	 * method below.
	 *
	 * @var string
	 */
	public $title = 'Example Module';

	/**
	 * The page header.
	 *
	 * You can set the page header statically here, or dynamically in the run
	 * method below.
	 *
	 * @var string
	 */
	public $header = 'Foo!';

	/**
	 * The page content.
	 *
	 * You can set the page content statically here, or dynamically in the run
	 * method below.
	 *
	 * @var string
	 */
	public $content;

	/**
	 * The footer text.
	 *
	 * You can set the footer text statically here, or dynamically in the run
	 * method below.
	 *
	 * @var string
	 */
	public $footer = 'Foo Example Module.';

	/**
	 * Custom styles for this module.
	 *
	 * You can set the custom styles statically here, or dynamically in the
	 * run method below. You must use $app->output->add_styles() regardless,
	 * or your styles will not be rendered.
	 *
	 * @var array
	 */
	private $styles = array(/* base64 encoded string */);

	/**
	 * Custom scripts for this module.
	 *
	 * You can set the custom scripts statically here, or dynamically in the
	 * run method below. You must use $app->output->add_scripts() regardless,
	 * or your scripts will not be rendered.
	 *
	 * @var array
	 */
	private $scripts = array(/* base64 encoded string */);

	/**
	 * Registers the module. Loads dependencies.
	 *
	 * This is where the module initialises itself. This includes setting
	 * navigation menu items or submenus. The page tile, header and content
	 * must not be set in this method. Defining this method is riquired.
	 *
	 * @return void
	 */
	public function __construct()
	{
		/* Add nav items to menu.
		 *
		 * You should add any menu items, submenus or submenu items here if not
		 * set statically in the class properties. These can be set in the run
		 * method if needed. Please note any menu items not added here will not
		 * be display unless this module is the active View.
		 *
		 * Add a menu item and a submenu:
		 *     $this->add_menu_item($item, $submenu);
		 *
		 * Add a menu item:
		 *     $this->add_nav_item($title, $priority, $submenu);
		 *
		 * Add a submenu:
		 *     $this->set_submenu($submenu);
		 *
		 * Add a submenu item:
		 *     $this->add_submenu_item($title, $queries);
		 *
		 * The first parameter, nav item $slug, should be the value of
		 * get_class(). Changing this with break the framework's routing.
		 */
		$this->add_nav_item(10, 'Foo Module', "#", array(
			array('href' => $this->getModuleURL(), 'title' => 'Foo'),
			array('href' => $this->getMethodURL('baz'), 'title' => 'Action baz'),
			array('href' => 'https://gitlab.com/jlareaux/fnf/wikis/home', 'title' => 'FnF Wiki'),
		));
	}

	/**
	 * Runs the module. Displays the output.
	 *
	 * This is where the module gets down to business. This can include setting
	 * the page title or header and adding custom styles or scripts. The module
	 * must set the page content and display the page in this method. If a post
	 * was submitted, the post data will be passed via the $post parameter.
	 * Defining this method is riquired.
	 *
	 * @uses Bar::output() Bar utility class example.
	 *
	 * @param array $post The http post if one was received.
	 * @return void
	 */
	public function index($post = null)
	{
		// Get the start time for the footer.
		$time_s = microtime();

		/**
		 * Bring the $app global into scope.
		 *
		 * The $app glabal object contains all core, module and helper classes.
		 * You can var_dump or print_r the $app variable to see what's inside.
		 *
		 * @global Application The main application class.
		 */
		global $app;

		/* Page Title Tag.
		 *
		 * You can set the page title tag statically in the properties, or you
		 * can do it here, dynamically.
		 *
		 * Using the class property:
		 *     $this->title = 'Foo';
		 *
		 * Using the Output class:
		 *     $app->output->set_title('Foo');
		 *
		 * Setting the page title using the Output class will override anything
		 * set via the class property.
		 */
		$app->output->set_title($this->title);

		/* Custom Styles.
		 *
		 * Adding styles using the Output class will be in addition to anything
		 * set via the class property.
		 */
		$app->output->add_styles($this->styles);

		/* Custom Scripts.
		 *
		 * Adding scripts using the Output class will be in addition to anything
		 * set via the class property.
		 */
		$app->output->add_scripts($this->scripts);

		/* Page Header.
		 *
		 * You can add a page header statically in the properties, or you can do
		 * it here dynamically.
		 *
		 * Using the class property:
		 *     $this->header = 'Foo';
		 *
		 * Using the Output class :
		 *     $app->output->set_header('Foo');
		 *
		 * Setting a page header using the Output class will override anything
		 * that may be set via the class property.
		 */
		$app->output->set_header($this->header);

		/* Load Utilities.
		 *
		 * Load any utilty class your module needs using the utility Loader
		 * class:
		 *     $app->libraries->load('Baz');
		 *
		 * Use the loaded utility:
		 *     $app->libraries->bar->property = 'foo';
		 * or:
		 *     $app->libraries->baz->method('foo');
		 */
		$app->libraries->load('Baz');

		/* Output.
		 *
		 * Do some stuff...
		 * whatever that is...
		 * then...
		 * generate the page content for this module.
		 */
		$output =
		'<p>This is the Foo module. Foo is a complete example of a module. ' .
		'The enitire module is a single class. Check out the source code of ' .
		'Foo to get started.</p><br />' . $app->libraries->baz->output();

		/* Page Content.
		 *
		 * You must set the page content here before the page is rendered.
		 *
		 * Using the class property:
		 *     $this->content = $content;
		 *
		 * Using the Output class :
		 *     $app->output->set_content($content);
		 *
		 * Setting the page content using the Output class will override anything that
		 * may be set via the class property.
		 */
		$app->output->set_content($output);

		// Get the end time for the footer.
		$time_e = microtime();

		/* Page Footer.
		 *
		 * You can add a page footer statically in the properties, or you can do
		 * it here dynamically.
		 *
		 * Using the class property:
		 *     $this->footer = $footer;
		 *
		 * Using the Output class :
		 *     $app->output->set_footer($footer);
		 *
		 * Adding a page footer using the Output class will override anything that
		 * may be set via the class property.
		 */
		$app->output->set_footer($this->footer . ' Module executed in ' . ($time_e - $time_s) . ' seconds.');

		/* Render page.
		 *
		 * The render method displays the page. It must used here or the script
		 * will not output the page.
		 */
		/** @todo Maybe do something like: $app->output->render($template_name); */
		$app->output->render();
	}

	/**
	 * Runs the Baz action. Displays the output.
	 *
	 * @param array  $post  The http post if one was received.
	 * @return void
	 */
	public function baz($post = null)
	{
		global $app;

		$app->output->set_title($this->title);

		$app->output->set_header($this->header);

		$app->output->set_content('<p>The Baz Method!</p>');

		$app->output->render();
	}

} // Foo
