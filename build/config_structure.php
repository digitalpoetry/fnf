<?php
/**
 * FnF Structure Configuration
 *
 * Configure the paths to the source, assets and project folders.
 *
 * NOTE: Paths MUST not have leading or trailing slashes.
 *
 * @package    DigitalPoetry\FnF\Build
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

/*
 * Project directories
 *
 * NOTE: Project paths MUST be relative from the project root direcUNINtory.
 */
$config['paths']['project'] =
[
	/*
	 * The application directory path. Contains source files used to
	 * compile the application.
	 */
	'application' => 'application',

	/*
	 * The build directory path. Contains the scripts used to compile the
	 * framework. If this path is changed, build_run.php must be edited as well.
	 */
	'build' => 'build',

	/*
	 * The documention directory path. Contains any auto-generated or
	 * hand-written documention about your project or third party libraries.
	 */
	'docs' => 'docs',

	/*
	 * The project system directory path. Contains source files used to compile
	 * the application.
	 */
	'system' => 'system',

	/*
	 * The project tests directory path. Contains the automated tests used when
	 * building the application.
	 */
	'tests' => 'tests',

	/*
	 * The theme directory path. Contains source files used to compile the
	 * application.
	 */
	'theme' => 'theme',

	/*
	 * The temporary directory path. Contains temporary files used by FnF to
	 * compile and build the application.
	 */
	'temp' => 'tmp',

	/*
	 * The Vendor directory path. Contains any third party libraries used during
	 * development only.
	 *
	 * Note: This directory will not be included in the compiled application.
	 */
	'vendor' => 'vendor'
];

/*
 * Source directories
 *
 * These directories are scanned at build time for source files to include in
 * the compiled application. Any file found in one of the below directories will
 * be complied and included in ths application at build time. You can add to
 * this array and your directory will be automagically included in thhe compiled
 * application.
 *
 * NOTE: Application source paths MUST be relative from the project application
 * directory.
 *
 * NOTE: System source paths MUST be relative from the project root directory, and
 * MUST have a path inside of the project system directory.
 */
$config['paths']['sources']['app'] =
[
	/*
	 * The application configuration directory path.
	 */
	'config' => 'config',

	/*
	 * The application core classes directory path.
	 */
	'core' => 'core',

	/*
	 * The application controllers directory path.
	 */
	'controllers' => 'controllers',

	/*
	 * The application helpers directory path.
	 */
	'helpers' => 'helpers',

	/*
	 * The application libraries directory path.
	 */
	'libraries' => 'libraries',

	/*
	 * The application modules directory path.
	 */
	'modules' => 'modules',

	/*
	 * The third party libraries directory path.
	 */
	'third-party' => 'third_party'
];

$config['paths']['sources']['system'] =
[
	/*
	 * The FnF configuration directory path.
	 */
	'config' => 'system' . DS . 'config',

	/*
	 * The FnF core classes directory path.
	 */
	'core' => 'system' . DS . 'core',

	/*
	 * The FnF file head & foot directory path.
	 */
	'file' => 'system',

	/*
	 * The FnF helpers directory path.
	 */
	'helpers' => 'system' . DS . 'helpers',

	/*
	 * The FnF libraries directory path.
	 */
	'libraries' => 'system' . DS . 'libraries'
];

/*
 * Asset directories
 *
 * These directories are scanned at build time for assets to embed in the
 * compiled source code.
 *
 * NOTE: Application asset paths MUST be relative from the project application
 * directory path.
 *
 * NOTE: Theme asset paths MUST be relative from the project root directory, and
 * MUST have a path inside of the project theme directory.
 */
$config['paths']['assets']['app'] =
[
	/*
	 * The application css directory path.
	 */
	'css'   => 'assets' . DS . 'css',

	/*
	 * The application images directory path.
	 */
	'img'   => 'assets' . DS . 'img',

	/*
	 * The application javascript directory path.
	 */
	'js'    => 'assets' . DS . 'js',

	/*
	 * The application views directory path.
	 */
	'views' => 'views'
];

$config['paths']['assets']['theme'] =
[
	/*
	 * The theme css directory path.
	 */
	'css' => 'theme' . DS . 'css',

	/*
	 * The theme image directory path.
	 */
	'img' => 'theme' . DS . 'img',

	/*
	 * The theme javascript directory path.
	 */
	'js' => 'theme' . DS . 'js',

	/*
	 * The theme views directory path.
	 */
	'views' => 'theme'
];

/*
 * Temporary directories
 *
 * These directories contain tempory or cached data. Do not put files you want
 * to keep in these dirrectories as they may be deleted.
 *
 * NOTE: Temporary paths MUST be relative from the project root directory.
 */
$config['paths']['temp'] =
[
	/*
	 * The cache directory path. Contains temporary and cache files for builds.
	 */
	'cache' => 'tmp' . DS . 'cache',

	/*
	 * The docs cache directory path. Contains cache files for documentation
	 * generation.
	 */
	'docs' => 'tmp' . DS . 'docs',

	/*
	 * The live run directory path. Contains temporary files used to run the
	 * application in live mode.
	 */
	'live'  => 'tmp' . DS . 'live',

	/*
	 * The log directory path. Contains logs generated during builds.
	 */
	'logs'  => 'tmp' . DS . 'logs',

	/*
	 * The Release directory path. Contains files resulting from release builds.
	 */
	'release'  => 'tmp' . DS . 'release'
];
