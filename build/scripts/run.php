<?php
/**
 * FnF Application Run Script
 *
 * Runs the application live or compiled. Running live has usable php error line
 * numbers and filepaths. Script Usage below.
 *
 * Run the application live (default):
 *     ?run=live
 * Run the application compiled:
 *     ?run=compiled
 *
 * @package    DigitalPoetry\FnF\Build
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

// Bring $config and $BuildApp into scope.
global $config, $BuildApp;

// Get the run mode.
$run_mode = isset($_GET['run']) ? $_GET['run'] : $config['build']['default_run_mode'];

// Run live or compiled?
if ($run_mode == 'live') {

	// Include the source files.
	foreach ($BuildApp->file_lists as $file_list) {
		foreach ($file_list as $file => $directory) {
			require(BASEPATH . DS . $BuildApp->paths['temp']['live'] . DS . $directory . DS . $file);
		}
	}

} else {

	// Include the compiled script.
	require $BuildApp->script_path . DS . $BuildApp->script_name;
}
