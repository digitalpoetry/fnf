<?php
/**
 * FnF Install Script.
 *
 * Installs the project directories.
 *
 * @package    DigitalPoetry\FnF\Build
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    http://opensource.org/licenses/MIT MIT License
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things <checksum>
 * @link       https://gitlab.com/jlareaux/fnf/tree/master
 * @filesource
 */

// Include build files.
require_once '../build_bootstrap.php';

// Verbose?
$verbose = isset($__GET['silent']) ? false : true;

// Set the install path.
# $path = BASEPATH;
$path = BASEPATH . DS . $config['paths']['temp']['release'];

// Instantiate the builder.
$builder = new BuildDirs($verbose);

// Install the project directories.
$builder->install($path);
