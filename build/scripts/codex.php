<?php
/**
 * FnF Generate Documentation Script
 *
 * Execute a shell command to commit to a git repository.
 *
 * @package    DigitalPoetry\FnF\Build
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

/*
 * phpdoc run [option1] [option2] [option3] ...
 *
 * CLI options:
 *
 * --target <path/to/target>              Path where to store the generated output.
 * --cache-folder <path/to/cache>         Path where to store the cache files.
 * --filename <path/to/file.ext>,<...>    Comma-separated list of files to parse. The wildcards ? and * are supported.
 * --directory <path/to/dir>,<...>        Comma-separated list of directories to (recursively) parse.
 * --encoding <encoding>                  encoding to be used to interpret source files with.
 * --extensions <extension>,<...>         Comma-separated list of extensions to parse, defaults to php, php3 and phtml.
 * --ignore <path/to/file.ext>,<...>      Comma-separated list of file(s) and directories (relative to the source-code
 *                                        directory) that will be ignored. Wildcards * and ? are supported.
 * --ignore-tags <tag>,<tag>,<...>        Comma-separated list of tags that will be ignored, defaults to none. package,
 *                                        subpackage and ignore may not be ignored..
 * --ignore-symlinks                      Ignore symlinks to other files or directories, default is on.
 * --markers <marker>,<...>               Comma-separated list of markers/tags to filter.
 * --visibility public,protected,private  Specifies the parse visibility that should be displayed in the documentation
 * --title <title>                        Sets the title for this project; default is the phpDocumentor logo.
 * --defaultpackagename <name>            Name to use for the default package. Default is 'Default''.
 * --template <name>                      Name of the template to use (optional). You can use a custom template by
 *                                        providing the path to it, relative to the current working directory.
 * --hidden                               Use this option to tell phpDocumentor to parse files and directories that
 *                                        begin with a period (.), by default these are ignored.
 * --force                                Forces a full build of the documentation, does not increment existing
 *                                        documentation.
 * --validate                             Validates every processed file using PHP Lint, costs a lot of performance.
 *                                        (comma separated e.g. "public,protected").
 * --sourcecode                           Whether to include syntax highlighted source code.
 * --progressbar                          Whether to show a progress bar; will automatically quiet logging to stdout.
 * --parseprivate                         Whether to parse DocBlocks marked with @internal tag.
 */

// DELETE ME
require_once dirname(__DIR__) . '/bootstrap.php';

// Bring $config into scope.
global $config;

// Get OS name
$exec = (stripos(php_uname('s'), 'windows') === false) ? 'shell_exec' : 'exec';
// Version number
$version = $config['build']['docblock']['version'];
// Config path.
$conf = BASEPATH . DS . 'phpdoc.dist.xml';
// Output path.
/** @todo Add config opt for below 'release' directory? */
$target = BASEPATH . DS . $config['paths']['project']['docs'] . DS . 'release';
// Cache path.
$cache = BASEPATH . DS . $config['paths']['temp']['docs'];
// Themes.
$themes = ['abstract', 'checkstyle', 'clean', 'responsive', 'xml', 'zend'];
// Vendor bin path.
$vendor_bin = BASEPATH . DS . $config['paths']['project']['vendor'] . DS . "bin";
// Log file path & header.
$log = BASEPATH . DS . $config['paths']['temp']['logs'] . DS . 'phpdoc_build_log.txt';
// Shell command.
$options_parse =
" --config {$conf}" .
" --cache-folder {$cache}" .
" --log {$log}" .
" --sourcecode" .
" --parseprivate" .
" --validate" .
" --force";
$options_transform =
" --config {$conf}" .
" --source {$source}" .
" --log {$log}";

// Change the working directory.
exec('cd ' . BASEPATH);

// Iterate phpdoc templates.
foreach ($themes as $theme) {

	// Log headers.
	$len        = strlen($theme) + 41;
	$hr_one     = str_repeat('=', $len);
	$hr_two     = str_repeat('-', $len);
	$date_time  = date("n/j/y h:i:s A");
	$log_header = "{$hr_one}\nDocumentation Build " . ucfirst($theme) . " {$date_time}\n{$hr_two}\n";

	// Add theme name to options & paths.
	$opt_template = ' --template ' . $theme;
	$opt_target = ' --target ' . $target . DS . '_themes' .  DS . $theme;

	// Generate docs.
	$stout_parse     = shell_exec("phpdoc parse" . $options_parse . $opt_template . $opt_target);
	$stout_transform = shell_exec("phpdoc transform" . $options_transform . $opt_template . $opt_target);

	// Write logs to file.
	$log_contents = $log_header . $stout_parse . "\n" . $stout_transform . "\n"/* . $stout_version . "\n"*/;
	file_put_contents($log, $log_contents, FILE_APPEND);
}
