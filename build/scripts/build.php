<?php
/**
 * FnF Application Build Script
 *
 * Builds the application. Script Usage below.
 *
 * Enable cache:
 *     ?cache=all
 * Enable cache by type:
 *     ?cache=files,assets,images
 * List source files to build from cache:
 *     ?cache=files&files=file1,file2,file3
 * Purge temporary files:
 *     ?purge=all
 * Purge temporary files by type:
 *     ?purge=cache,live,release
 * Purge temporary files and die:
 *     ?purge=all,die
 *
 * @package    DigitalPoetry\FnF\Build
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

// Instantiate the application builder.
$BuildApp = new BuildApp;

// Compile the application source files.
$BuildApp->compile();

// Write the compile application to file.
$BuildApp->writeScript();
