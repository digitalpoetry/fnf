<?php
/**
 * FnF Post Build Script.
 *
 * This script is ran after each build.
 *
 * @package    DigitalPoetry\FnF\Build
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    http://opensource.org/licenses/MIT MIT License
 * @version    0.1.0 Basic Things 
 * @since      0.1.0 Basic Things <checksum>
 * @link       https://gitlab.com/jlareaux/fnf/tree/master
 * @filesource
 */

// Include build files.
require_once '../build_bootstrap.php';
