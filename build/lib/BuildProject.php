<?php
/**
 * FnF Project Builder
 *
 * A tool that installs the configured project directories.
 *
 * @package    DigitalPoetry\FnF\Build
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things 
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

/**
 * The BuildProject class.
 *
 * Utility that installs project directories.
 *
 * @package  DigitalPoetry\FnF\Build
 * @author   Jesse LaReaux <jlareaux@gmail.com>
 * @since    0.1.0 Basic Things
 *
 * @todo Add a .gitkeep file to empty directories.
 */
class BuildProject extends BaseBuilder
{
	/**
	 * The list of project directories.
	 *
	 * @var array
	 */
	public $paths;

	/**
	 * Runs the Install script
	 *
	 * @return void
	 */
	public function __construct()
	{
		// Installer config.
		global $config;
		$this->paths = $config['paths'];
    }

	/**
	 * Ensures the configured directory structure exists at the given path.
	 *
	 * @param string  $path  Path to install to.
	 * @return mixed  Outputs progress to the page if verbose is true.
	 */
	public function install($path)
	{
		// Set the installation path.
		$base_path = empty($path) ? BASEPATH : $path;

		// Get each group of paths.
		foreach ($this->paths as $group => $path_list) {
			foreach ($path_list as $package => $path) {

				/*
				 * Is a single path?
				 */
				if (is_string($path)) {

					// Install the directory to the install path.
					if (! $this->ensureFolder($base_path . DS . $path)) {
						$this->abort('Cannot create directory "", check permissions.');
					}
				}

				/*
				 * Is a list of paths?
				 */
				if (is_array($path)) {

					// Get each path in the group.
					$paths = $path;
					foreach ($paths as $id => $path) {

						// Prepend Application paths.
						if ($package == 'app') {
							$path = $this->paths['project']['application'] . DS . $path;
						}

						// Install the directory to the install path.
						$path = $base_path . DS . $path;
						if (! $this->ensureFolder($path)) {
							$this->abort("Cannot create directory '{$path}', check permissions.");
						}
					}
				}
			}
		}
	}

	/**
	 * Generates a salt for hashing functions.
	 *
	 * @param string $length  Length of the salt to generate.
	 * @return string  The generated salt.
	 */
	public function generateSalt($length = 80)
	{
		$salt = "";
		for ($i = 0; $i < $length; $i++) {
			$chars = str_shuffle("abcdefghijklmnopqrstuvwxyzABCDEFGHIJKLMNOPQRSTUVWXYZ0123456789!@#$%^&*()_-=+;:,.?");
			$salt .= $chars[rand(1, strlen($chars) - 1)];
		}
		return $salt;
	}

} // BuildProject
