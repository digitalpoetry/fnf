<?php
/**
 * The BuildDirs File
 *
 * A tool that installs the configured project directories.
 *
 * Browser usage:
 *     /BuildDirs.php?verbose
 *
 * Object Oriented Usage:
 *     $path = BASEPATH . DS . $config['paths']['temp']['release']; // Set the install path.
 *     $builder = new BuildDirs(true); // Verbose output.
 *     $builder->install($path); // Run the install.
 *
 * @package    DigitalPoetry\FnF\Build
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    http://opensource.org/licenses/MIT MIT License
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things <checksum>
 * @link       https://gitlab.com/jlareaux/fnf/tree/master
 * @filesource
 */

// Include build files.
require_once '../build_bootstrap.php';

/**
 * The BuildDirs class.
 *
 * Utility that installs project directories.
 *
 * @package  DigitalPoetry\FnF\Build
 * @author   Jesse LaReaux <jlareaux@gmail.com>
 * @since    0.1.0 Basic Things <checksum>
 *
 * @todo Add a .gitkeep file to empty directories.
 */
class BuildDirs extends BaseBuilder
{
	/**
	 * The list of project directories.
	 *
	 * @var array
	 */
	public $paths;

	/**
	 * The path to install the directories to.
	 *
	 * @var string
	 */
	public $install_path;

	/**
	 * Runs the Install script
	 *
	 * @return void
	 */
	public function __construct()
	{
		// Installer config.
		global $config;
		$this->paths = $config['paths'];
    }

	/**
	 * Ensures the configured directory structure exists at the given path.
	 *
	 * @param str  $install_path  Path to install to.
	 * @return mixed  Outputs progress to the page if verbose is true.
	 */
	public function install($install_path)
	{
		// Set the installation path.
		$this->install_path = empty($install_path) ? BASEPATH : $install_path;

		// Get each group of paths.
		//
		// Could the below loops be a single loop?
		foreach ($this->paths as $group => $path_list) {
			foreach ($path_list as $package => $path) {

				/*
				 * Is a single path?
				 */
				if (is_string($path)) {

					// Install the directory to the install path.
					if (! $this->ensureFolder($this->install_path . DS . $path)) {
						die('Cannot create directory "", check permissions.');
					}
				}

				/*
				 * Is a list of paths?
				 */
				if (is_array($path)) {

					// Get each path in the group.
					$paths = $path;
					foreach ($paths as $id => $path) {

						// Prepend Application paths.
						if ($package == 'app') {
							$path = $this->paths['project']['application'] . DS . $path;
						}

						// Install the directory to the install path.
						$path = $this->install_path . DS . $path;
						if (! $this->ensureFolder($path)) {
							die("Cannot create directory '{$path}', check permissions.");
						}
					}
				}
			}
		}
	}

} // BuildDirs
