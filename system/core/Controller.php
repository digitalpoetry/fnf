<?php
/**
 * Framework in a File.
 *
 * The micro framework in a single file. Quickly develop and deploy scripts and
 * applications.
 *
 * @package    DigitalPoetry\FnF\System\Core
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

/**
 * Controller base class.
 *
 * Module controllers should extend this class. The `__construct()` function should
 * register the module, and the `run()` function should display the module's
 * output.
 *
 * @package  DigitalPoetry\FnF\Library
 * @author   Jesse LaReaux <jlareaux@gmail.com>
 * @since    0.1.0 Basic Things
 */
abstract class Controller
{
	/**
	 * The default method to run if not supplied as a query parameter.
	 *
	 * @var   string
	 */
	public $default_method = 'index';

	/**
	 * Registers the module. Loads dependencies.
	 *
	 * @return void
	 * @todo Decide if this is needed. IE, a single module app that only wants
	 *       the brand in the navbar.
	 */
	abstract public function __construct();

	/**
	 * Adds a navigation item.
	 *
	 * @param integer  $priority  The menu order priority.
	 * @param string   $title     The title for this menu item.
	 * @param array    $href      Optional. The nav link href. Default is the
	 *                            string '#'.
	 * @param array    $submenu   Optional. The submenu items. Default is an
	 *                            empty array.
	 * @return void
	 */
	final protected function add_nav_item($priority, $title, $href = '#', $submenu = array()) {

		// Bring the $app global into scope.
		global $app;

		// Add nav item to menu.
		$app->output->add_nav_item(get_class($this), $priority, $title, $href, $submenu);
	}

	/**
	 * Sets a submenu.
	 *
	 * Set the submenu for a navigation item. Please note that this will
	 * over-write anything set already.
	 *
	 * @param array  $items  The submenu items.
	 * @return void
	 */
	final protected function set_submenu($items) {

		// Bring the $app global into scope.
		global $app;

		// Add nav item to menu.
		$app->output->set_submenu(get_class($this), $items);
	}

	/**
	 * Adds an item to a submenu.
	 *
	 * @param string  $title  The title for this menu item.
	 * @param array   $href   The nav link href.
	 * @return void
	 */
	final protected function add_submenu_item($title, $href = '#')
	{
		// Bring the $app global into scope.
		global $app;

		// Add nav item to menu.
		$app->output->add_submenu_item(get_class($this), $title, $href);
	}

	/**
	 * Gets the module url.
	 *
	 * @return string  The module url.
	 */
	final protected function getModuleURL()
	{
		// Bring $app into scope.
		global $app;

		// Return the module url.
		return  $app->url . '?' . $app->getConfig('queryparam.module', 'system') . '=' . strtolower(get_class($this));
	}

	/**
	 * Gets the method url.
	 *
	 * @param  string  $method  The method to get a url for.
	 * @return string  The method url.
	 */
	final protected function getMethodURL($method)
	{
		// Bring $app into scope.
		global $app;

		// Return the module url with the given method parameter.
		return  $this->getModuleURL() . '&' . $app->getConfig('queryparam.method', 'system') . '=' . $method;
	}

} // Controllers
