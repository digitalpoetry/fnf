<?php
/**
 * Framework in a File.
 *
 * The micro framework in a single file. Quickly develop and deploy scripts and
 * applications.
 *
 * @package    DigitalPoetry\FnF\System\Core
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

/**
 * Application Core Class.
 *
 * The Application class that serves as the front controller provides easy
 * access to core and utility classes.
 *
 * @package  DigitalPoetry\FnF\Core
 * @author   Jesse LaReaux <jlareaux@gmail.com>
 * @since    0.1.0 Basic Things
 */
class Application
{
	/**
	 * The application url.
	 *
	 * @var string
	 */
	public $url;

	/**
	 * The application uri with only core query parameters.
	 *
	 * @var string
	 */
	public $base_uri;

	/**
	 * The application uri with all request query parameters.
	 *
	 * @var string
	 */
	public $request_uri;

	/**
	 * A salted hash to verify requests with.
	 *
	 * @var string
	 * @todo Find a better home for this property.
	 */
	private $secure_token;

	/**
	 * The application configuration.
	 *
	 * @var string
	 */
	private $config;

	/**
	 * Holds the Route class object.
	 *
	 * @var Route
	 * @todo Make $route private.
	 */
	public $route;

	/**
	 * Holds the Output class object.
	 *
	 * @var Output
	 */
	public $output;

	/**
	 * Holds helper class objects.
	 *
	 * @var mixed
	 */
	public $helpers;

	/**
	 * Holds module class objects.
	 *
	 * @var mixed
	 */
	public $modules;

	/**
	 * Holds library class objects.
	 *
	 * @var mixed
	 */
	public $libraries;

	/**
	 * Holds the active module class object.
	 *
	 * @var mixed
	 */
	public $module;

	/**
	 * Starts up the application.
	 *
	 * @return void
	 */
	public function initialise()
	{
		// Configuration.
		global $config;
		$this->config = $config;

		// Set the script url and uri.
		$this->set_urls();

		// Set the secure token.
		$this->generate_secure_token();
	}

	/**
	 * Runs the application.
	 *
	 * Routes the current view to and runs the associated module.
	 *
	 * @return void
	 */
	public function run()
	{
		// Set the active module.
		$this->route->request();

		// Run the module.
		$method = $this->route->method;
		$this->module->$method();
	}

	/**
	 * Sets the base and requested uri for the script.
	 *
	 * The base uri contains core query parameters only. The requested uri
	 * contains all query parameters from the request.
	 *
	 * @return void
	 */
	private function set_urls()
	{
		// Get the query parameter names and default module and method.
		$queryparam_module = $this->getConfig('queryparam.module', 'system');
		$queryparam_method = $this->getConfig('queryparam.method', 'system');
		$default_module    = $this->getConfig('default_module', 'system');

		// Build the query parameter strings.
		$core_params    = isset($_GET[$queryparam_module]) ? '?' . $queryparam_module . '=' . $_GET[$queryparam_module] : '';
		$core_params   .= isset($_GET[$queryparam_method]) ? '?' . $queryparam_method . '=' . $_GET[$queryparam_method] : '';
		$request_params = empty($_SERVER['QUERY_STRING']) ? '' : '?' . $_SERVER['QUERY_STRING'];

		// Base url
		$url = $this->getConfig('app', 'url');
		if (empty($url)) {
			$this->url =
			$_SERVER['REQUEST_SCHEME'] . '://' .
			$_SERVER['HTTP_HOST'] .
			$_SERVER['SCRIPT_NAME'];
		} else {
			$this->url = $url;
		}

		// Base uri.
		$this->base_uri =
		$this->url .
		$core_params;

		// Requested uri.
		$this->request_uri =
		$this->url .
		$request_params;
	}

	/**
	 * Generates the secure token for verifying requests.
	 *
	 * @return void
	 */
	private function generate_secure_token()
	{
		$this->secure_token = sha1(
			$this->getConfig('secure_salt', 'system') .
			$_SERVER['HTTP_HOST'] .
			$_SERVER['SCRIPT_FILENAME'] .
			$_SERVER['SERVER_NAME'] .
			$_SERVER['SERVER_SIGNATURE']
		);
	}

	/**
	 * Gets a configuration value.
	 *
	 * @param string $name  Name of the configuration value.
	 * @param string $group The configuratione group.
	 * @param string $value The configuration value.
	 * @return void
	 */
	public function setConfig($name, $group, $value)
	{
		$this->config[$group][$name] = $value;
	}

	/**
	 * Gets a configuration value.
	 *
	 * @param string $name  Name of the configuration value.
	 * @param string $group The configuration group.
	 * @return string The configuration value or false if not set.
	 */
	public function getConfig($name, $group)
	{
		if (isset($this->config[$group]) && isset($this->config[$group][$name])) {
			return $this->config[$group][$name];
		} else {
			return false;
		}
	}

} // Application
