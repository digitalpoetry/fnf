<?php
/**
 * Framework in a File.
 *
 * The micro framework in a single file. Quickly develop and deploy scripts and
 * applications.
 *
 * @package    DigitalPoetry\FnF\System\Core
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    http://opensource.org/licenses/MIT MIT License
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things <checksum>
 * @link       https://gitlab.com/jlareaux/fnf/tree/master
 * @filesource
 */

/*
 * Configuration
 */

/**
 * The application name.
 */
$config['site']['name'] = 'Framework in a File';

/**
 * The application nickname.
 */
$config['site']['nickname'] = 'FnF';

/**
 * The application admin email.
 */
# $config['site']['notifications_email'] = 'jlareaux@gmail.com';

/**
 * Default module to use if the module query parameter is not supplied.
 */
$config['app']['default_module'] = 'foo';

/**
 * Default module query parameter name.
 */
$config['app']['queryparam.module'] = 'view';

/**
 * Default method query parameter name.
 */
$config['app']['queryparam.method'] = 'action';
