<?php
/**
 * Framework in a File.
 *
 * The micro framework in a single file. Quickly develop and deploy scripts and
 * applications.
 *
 * @package    DigitalPoetry\FnF\System\Helper
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

/**
 * Text Helper Class.
 *
 * @package  DigitalPoetry\FnF\Helper
 * @author   Jesse LaReaux <jlareaux@gmail.com>
 * @since    0.1.0 Basic Things
 */
class Text
{
	/**
	 * Convert bytes into a human readble string
	 *
	 * @param int|str $bytes    Required. The bytesize.
	 * @param integer $decimals Optional. The number of decimals. Default is 2.
	 * @return string A friendly filesize string.
	 */
	public function human_filesize($bytes, $decimals = 2)
	{
		$sizes = array('bytes', 'KB', 'MB', 'GB', 'TB', 'PB');
		$factor = floor((strlen($bytes) - 1) / 3);
		$size = $bytes == 0 ? "0" : sprintf("%.{$decimals}f", $bytes / pow(1024, $factor));
		return $size . " " . @$sizes[$factor];
	}

	/**
	 * Convert a timestamp into a human readble string.
	 *
	 * @see http://php.net/manual/en/function.date.php.
	 *
	 * @param int|str $timestamp Required. The timestamp to convert.
	 * @param string  $format    Optional. The date format. Default is
	 *                           "M j, Y, g:i a".
	 * @return string A friendly date string.
	 */
	public function human_date($timestamp, $format = "M j, Y, g:i a")
	{
		return date($format, $timestamp);
	}

	/**
	 * Convert file permissions into a human readble string.
	 *
	 * @see http://php.net/manual/en/function.fileperms.php.
	 *
	 * @param int|str $perms Required. The permissions to convert.
	 * @return string A friendly permissions string.
	 */
	public function human_permissions($perms)
	{
		return substr(sprintf('%o', $perms), -4);
	}

	/**
	 * Space string helper.
	 *
	 * Returns a string with the given number of space characters.
	 *
	 * @param integer $spaces Required. The number spaces characters.
	 * @return string Returns a string of space characters.
	 *
	 * @todo Remove this function if not used.
	 */
	public function s($spaces)
	{
		return str_repeat(" ", $spaces);
	}

	/**
	 * Tab string helper.
	 *
	 * Returns a string with the given number of tab characters.
	 *
	 * @param integer $tabs Required. The number tabs characters.
	 * @return string Returns a string of tab characters.
	 *
	 * @todo Remove this function if not used.
	 */
	public function t($tabs)
	{
		return str_repeat("\t", $tabs);
	}

	/**
	 * Pads a string of text.
	 *
	 * Indents and appends a line feed character to the given string.
	 *
	 * @param integer $tabs      Required. The number of tabs to indent the
	 *                           string.
	 * @param string  $string    Required. The string of html text to pad.
	 * @param boolean $use_space Optional. Use space characters instead of tabs.
	 *                           Default is false.
	 * @return string The padded line of html.
	 */
	public function p($tabs, $string, $use_space = false)
	{
		$ws = ($use_space == true) ? " " : "\t";
		return str_repeat($ws, $tabs) . $string . PHP_EOL;
	}

	/**
	 * Pads a string of text until the given string length.
	 *
	 */
	public function pad_to($length, $string, $append = false)
	{
		$len = strlen($string);
		$pad = $length - $len;

		if ( $pad <= 0 ) {
			return $string;
		}
			
		if ( $append == true ) {
			$string = $string .str_repeat('&nbsp', $pad);
		} else {
			$string = str_repeat('&nbsp', $pad) . $string;
		}

		return $string;
	}

} // Text
