<?php
/**
 * Framework in a File.
 *
 * The micro framework in a single file. Quickly develop and deploy scripts and
 * applications.
 *
 * @package    DigitalPoetry\FnF\System\Helpers
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */


/**
 * Debug Helper Class.
 *
 * Provides a UIKit designed to work with the Bootstrap 3.2 CSS Framework.
 *
 * @package  DigitalPoetry\FnF\Helper
 * @author   Jesse LaReaux <jlareaux@gmail.com>
 * @since    0.1.0 Basic Things
 */
class Debug
{
	/**
	 * Logs variables in the browser console instead of dumping on the page.
	 *
	 * @param mixed $data The variable to log.
	 */
	public function console_log($data)
	{
		echo
		'<script>',
		'console.log(' . json_encode( $data ) . ')',
		'</script>';
	}

} // Debug
