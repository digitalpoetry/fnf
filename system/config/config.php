<?php
/**
 * Framework in a File.
 *
 * The micro framework in a single file. Quickly develop and deploy scripts and
 * applications.
 *
 * @package    DigitalPoetry\FnF\System\Config
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

/**
 * Default module to use if the module query parameter is not supplied.
 */
$config['system']['default_module'] = 'foo';

/**
 * Default module query parameter name.
 */
$config['system']['queryparam.module'] = 'view';

/**
 * Default method query parameter name.
 */
$config['system']['queryparam.method'] = 'action';

/**
 * Salt used to generate secure tokens for validating requests.
 */
$config['system']['secure_salt'] = '<salt_me>';
