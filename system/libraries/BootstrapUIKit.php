<?php
/**
 * Framework in a File.
 *
 * The micro framework in a single file. Quickly develop and deploy scripts and
 * applications.
 *
 * @package    DigitalPoetry\FnF\System\Libraries
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */


/**
 * Class Bootstrap3 UIKit
 *
 * Provides a UIKit designed to work with the Bootstrap 3.2 CSS Framework.
 *
 * @package  DigitalPoetry\FnF\Helper
 * @author   Jesse LaReaux <jlareaux@gmail.com>
 * @since    0.1.0 Basic Things
 */
class BootstrapUIKit extends UIKit {

	/**
	 * Name of the UI Kit.
	 *
	 * @var string
	 */
	protected $name = 'Bootstrap3';

	/*
	 * Grid
	 */

	/**
	 * Creates a row wrapper of HTML.
	 *
	 * @param array    $options Optional. Associative array of custom options
	 *                          and any standard tag attribute for the tag.
	 *                          Default is empty array.
	 * @param callable $c       A callback to parse the inner html.
	 * @return string Row wrapper html.
	 */
	public function row($options = [], \Closure $c)
	{
		// Parse attributes.
		list($classes, $id, $attributes) = $this->parseStandardOptions($options, 'row', true);

		// Open the row.
		$output = "<div {$id} {$classes} {$attributes}>\n";

		// Do any callbacks.
		$output .= $this->runClosure($c);

		// Close the row.
		$output .= "</div>\n";

		return $output;
	}

	/**
	 * Creates the CSS for a column in a grid.
	 *
	 * The attribute array is made up of key/value pairs with the
	 * key being the size, and the value being the number of columns/offset
	 * in a 12-column grid.
	 *
	 * Note that we currently DO NOT support offset columns.
	 *
	 * Valid sizes - 'xs', 'sm', 'md', 'lg', 'xs-offset', 'sm-offset', 'md-offset', 'lg-offset'
	 *
	 * @param array    $options Optional. Associative array of custom options
	 *                          and any standard tag attribute for the tag.
	 *                          Default is empty array.
	 * @param callable $c       A callback to parse the inner html.
	 * @return string Column wrapper html.
	 * @todo Edit this Docblock: "currently DO NOT support offset columns".
	 */
	public function column($options = [], \Closure $c)
	{
		// Build our classes
		$classes = '';

		foreach ($options['sizes'] as $size => $value)
		{
			switch ($size)
			{
				case 'xs':
					$classes .= ' col-xs-'. $value;
					break;
				case 'sm':
					$classes .= ' col-sm-'. $value;
					break;
				case 'md':
					$classes .= ' col-md-'. $value;
					break;
				case 'lg':
					$classes .= ' col-lg-'. $value;
					break;
				case 'xs-offset':
					$classes .= ' col-xs-offset-'. $value;
					break;
				case 'sm-offset':
					$classes .= ' col-sm-offset-'. $value;
					break;
				case 'md-offset':
					$classes .= ' col-md-offset-'. $value;
					break;
				case 'lg-offset':
					$classes .= ' col-lg-offset-'. $value;
					break;
			}
		}

		// Parse attributes.
		list($classes, $id, $attributes) = $this->parseStandardOptions($options, $classes, true);

		// Open the column.
		$output = "<div {$id} {$classes} {$attributes}>\n";

		// Do any callbacks.
		$output .= $this->runClosure($c);

		// Close the column.
		$output .= "</div>\n";

		return $output;
	}

	/*
	 * Navigation
	 */

	/**
	 * Generates the container code for a navbar, typically used along the top
	 * of a page.
	 *
	 * @param array    $options Optional. Associative array of custom options
	 *                          and any standard tag attribute for the tag.
	 *                          Default is empty array.
	 * @param callable $c       A callback to parse the inner html.
	 * @return string           Navbar html.
	 */
	public function navbar($options = [], \Closure $c)
	{
		// Set base class.
		$classes = "navbar";

		// Set optional classes.
		foreach ($options as $option)
		{
			switch ($option)
			{
				case 'default':
					$classes .= " navbar-default";
					break;
				case 'inverse':
					$classes .= " navbar-inverse";
					break;
				case 'fixed':
					$classes .= " navbar-fixed-top";
					break;
				case 'static':
					$classes .= " navbar-static-top";
					break;
			}
		}

		// Parse attributes.
		list($class, $id, $attributes) = $this->parseStandardOptions($options, $classes, true);

		// Open the navbar.
		$output =
		"<nav {$class} {$id} {$attributes} role='navigation'>\n" .
		"\t<div class='container'>\n";

		// Do any callbacks.
		$output .= $this->runClosure($c);

		// Close the navbar.
		$output .= "\t</div>\n</nav>\n";

		return $output;
	}

	/**
	 * Builds the HTML for the Title portion of the navbar. This typically
	 * includes the code for the hamburger menu on small resolutions.
	 *
	 * @param string $title Navbar brand link text.
	 * @param string $url   Optional. Navbar brand link url. Default is '#'.
	 * @return string       Navbar brand html.
	 */
	public function navbarBrand($title, $url='#')
	{
		// Output the navbar title.
		return '
		<div class="navbar-header">
			<button type="button" class="navbar-toggle" data-toggle="collapse" data-target="#navbar" aria-expanded="false" aria-controls="navbar">
				<span class="sr-only">Toggle navigation</span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
				<span class="icon-bar"></span>
			</button>
			<a class="navbar-brand" href="'. $url .'">'. $title .'</a>
		</div>';
	}

	/**
	 * Creates a Nav UL. Can be used for any menu including the navbar.
	 *
	 * @param array    $options Optional. Associative array of custom options
	 *                          and any standard tag attribute for the tag.
	 *                          Default is empty array.
	 * @param callable $c    A callback to parse the inner html.
	 * @return string        Nav menu html.
	 */
	public function nav($options = [], \Closure $c)
	{
		// Parse attributes.
		list($classes, $id, $attributes) = $this->parseStandardOptions($options, "nav", true);

		// Open the nav.
		$output = "\t<ul {$id} {$classes} {$attributes}>\n";

		// Do any callbacks.
		$output .= $this->runClosure($c);

		// Close the nav.
		$output .= "\t</ul>\n";

		return $output;
	}

	/**
	 * Creates a single list item for use within a nav section.
	 *
	 * @param string $title     Nav item title.
	 * @param string $url       Optional. Nav item title. Default is '#'.
	 * @param array  $options   Optional. Associative array of custom options
	 *                          and any standard tag attribute for the tag.
	 *                          Default is empty array.
	 * @param boolean $isActive Optional. Indicates the active nav item. Default
	 *                          is '#'.
	 * @return string Nav item html.
	 */
	public function navItem($title, $url='#', $options = [], $isActive = false)
	{
		// Set the current nav item.
		$this->states['activeNavTitle'] = $title;

		// Set nav item active class.
		$class = $isActive ? $this->active_class : '';

		// Parse attributes.
		list($classes, $id, $attributes) = $this->parseStandardOptions($options, $class, true);

		return "\t<li {$id} {$classes} {$attributes}><a href='{$url}'>{$title}</a></li>\n";
	}

	/**
	 * Builds the shell of a Dropdown button for use within a nav area.
	 *
	 * @param string   $title   Dropdown title.
	 * @param array    $options Optional. Associative array of custom options
	 *                          and any standard tag attribute for the tag.
	 *                          Default is empty array.
	 * @param callable $c       Optional. A callback to parse the inner html.
	 * @return string           Nav dropdown html.
	 */
	public function navDropdown($title, $options = [], \Closure $c)
	{
		// Parse attributes.
		list($classes, $id, $attributes) = $this->parseStandardOptions($options, 'dropdown', true);

		// Open the nav dropdown.
		$output = "<li {$id} {$classes} {$attributes}>\n" .
		"\t<a href='#' class='dropdown-toggle' data-toggle='dropdown'>{$title} <span class='caret'></span></a>\n" .
		"\t<ul class='dropdown-menu' role='menu'>\n";

		// Do any callbacks.
		$output .= $this->runClosure($c);

		// Close the nav dropdown.
		$output .= "\t</ul></li>\n";

		return $output;
	}

	/**
	 * Creates a divider for use within a nav list.
	 *
	 * @return string Nav divider html.
	 */
	public function navDivider()
	{
		// Output the divider.
		return "<li class=\"divider\"></li>\n";
	}

	/**
	 * Creates a nav menu for use within a sidebar.
	 *
	 * @param array    $options Optional. Associative array of custom options
	 *                          and any standard tag attribute for the tag.
	 *                          Default is empty array.
	 * @param callable $c       Optional. A callback to parse the inner html.
	 * @return string Nav divider html.
	 */
	public function sideNav($options = [], \Closure $c)
	{
		// Parse attributes.
		list($classes, $id, $attributes) = $this->parseStandardOptions($options, 'nav nav-pills nav-stacked', true);

		// Open the nav divider.
		$output = "<ul {$id} {$classes} {$attributes}>\n";

		// Do any callbacks.
		$output .= $this->runClosure($c);

		// Close the nav divider.
		$output .= "</ul>\n";

		return $output;
	}

	/**
	 * Creates a list of nav items to function as breadcrumbs for a site.
	 *
	 * @param array    $options Optional. Associative array of custom options
	 *                          and any standard tag attribute for the tag.
	 *                          Default is empty array.
	 * @param callable $c       Optional. A callback to parse the inner html.
	 * @return string  Breadcrumbs html.
	 */
	public function breadcrumb($options = [], \Closure $c)
	{
		// Parse attributes.
		list($classes, $id, $attributes) = $this->parseStandardOptions($options, 'breadcrumb', true);

		// Open the breadcrumbs.
		$output = "<ol {$id} {$classes} {$attributes}>\n";

		// Do any callbacks.
		$output .= $this->runClosure($c);

		// Close the breadcrumbs.
		$output .= "</ol>\n";

		return $output;
	}

	/**
	 * Tables
	 *
	 * Creates a table.
	 *
	 * @return mixed
	 */
	public function table()
	{
		return 'table';
	}

	/*
	 * Buttons
	 */

	/**
	 * Creates a simple button.
	 *
	 * @param string $title    Button title.
	 * @param string $style    Optional. Button style class. Accepted values are
	 *                         default, primary, success, info, warning, danger.
	 *                         Default is 'default'.
	 * @param string $size     Optional. Button size class. Accepted values are
	 *                         default, small, xsmall, large. Default is
	 *                         'default'.
	 * @param array  $options  Optional. Associative array of custom options and
	 *                         any standard element attribute for the tag.
	 * @return string Button html.
	 */
	public function button($title, $style='default', $size='default', $options = [])
	{
		$tag= "<button type='button' {classes} {id} {attributes}>{$title}</button>";

		return $this->renderButtonElement($title, $style, $size, $options, $tag);
	}

	/**
	 * Creates a simple link styled as a button.
	 *
	 * @param string $title    Button title.
	 * @param string $url      Optional. Button link.
	 * @param string $style    Optional. Button style class. Accepted values are
	 *                         default, primary, success, info, warning, danger.
	 *                         Default is 'default'.
	 * @param string $size     Optional. Button size class. Accepted values are
	 *                         default, small, xsmall, large. Default is
	 *                         'default'.
	 * @param array  $options  Optional. Associative array of custom options and
	 *                         any standard element attribute for the tag.
	 * @return string Button link html.
	 */
	public function buttonLink($title, $url='#', $style='default', $size='default', $options = [])
	{
		// Output the button link.
		$tag = "<a href='{$url}' {classes} {id} {attributes} role='button'>{$title}</a>";

		return $this->renderButtonElement($title, $style, $size, $options, $tag);
	}

	/**
	 * Helper method to render out our buttons in a DRY manner.
	 *
	 * @param string $title    Button title.
	 * @param string $style    Optional. Button style class. Accepted values are
	 *                         default, primary, success, info, warning, danger.
	 *                         Default is 'default'.
	 * @param string $size     Optional. Button size class. Accepted values are
	 *                         default, small, xsmall, large. Default is
	 *                         'default'.
	 * @param array  $options  Optional. Associative array of custom options and
	 *                         any standard element attribute for the tag.
	 * @param string Button html.
	 */
	protected function renderButtonElement($title, $style, $size, $options, $tag)
	{
		$valid_styles = ['default', 'primary', 'success', 'info', 'warning', 'danger'];
		$valid_sizes  = ['default', 'small', 'xsmall', 'large'];

		if (! in_array($style, $valid_styles))
		{
			$style = 'default';
			$options['attributes'][] = 'data-error="Invalid Style passed to button method."';
		}

		$classes = 'btn ';

		// Sizes
		switch($size)
		{
			case 'small':
				$classes .= 'btn-sm ';
				break;
			case 'xsmall':
				$classes .= 'btn-xs ';
				break;
			case 'large':
				$classes .= 'btn-lg ';
				break;
			default:
				break;
		}

		// Styles
		switch ($style)
		{
			case 'primary':
				$classes .= 'btn-primary ';
				break;
			case 'success':
				$classes .= 'btn-success ';
				break;
			case 'info':
				$classes .= 'btn-info ';
				break;
			case 'warning':
				$classes .= 'btn-warning ';
				break;
			case 'danger':
				$classes .= 'btn-danger ';
				break;
			case 'default':
				$classes .= 'btn-default ';
				break;
		}

		// Parse attributes.
		list($classes, $id, $attributes) = $this->parseStandardOptions($options, $classes, true);

		$tag = str_replace('{classes}', $classes, $tag);
		$tag = str_replace('{id}', $id, $tag);
		$tag = str_replace('{attributes}', $attributes, $tag);
		$tag = str_replace('{title}', $title, $tag);

		return $tag;
	}

	/**
	 * Creates button groups wrapping HTML.
	 *
	 * @param array $options  Optional. Associative array of custom options
	 *                        and any standard tag attribute for the tag.
	 *                        Default is empty array.
	 * @param callable $c     Optional. A callback to parse the inner html.
	 * @return string  Button group html.
	 */
	public function buttonGroup($options, \Closure $c)
	{
		// Parse attributes.
		list($classes, $id, $attributes) = $this->parseStandardOptions($options, 'btn-group', true);

		// Open the button group.
		$output = "<div {$id} {$classes} {$attributes}>\n";

		// Do any callbacks.
		$output .= $this->runClosure($c);

		// Close the button group.
		$output .= "</div>\n";

		return $output;
	}

	/**
	 * Creates the button bar wrapping HTML.
	 *
	 * @param array    $options Optional. Associative array of custom options
	 *                          and any standard tag attribute for the tag.
	 *                          Default is empty array.
	 * @param callable $c       Optional. A callback to parse the inner html.
	 * @return string  Button bar html.
	 */
	public function buttonBar($options, \Closure $c)
	{
		// Default attributes.
		$options['attributes'][] = 'role="toolbar"';

		// Parse attributes.
		list($classes, $id, $attributes) = $this->parseStandardOptions($options, 'btn-toolbar', true);

		// Open the button bar.
		$output = "<div {$id} {$classes} {$attributes}>\n";

		// Do any callbacks.
		$output .= $this->runClosure($c);

		// Close the button bar.
		$output .= "</div>\n";

		return $output;
	}

	/**
	 * Creates a button that also has a dropdown menu. Also called Split Buttons
	 * by some frameworks.
	 *
	 * @param string   $title    Button title text.
	 * @param string  $style     Optional. Button style class. Accepted values
	 *                           are default, primary, success, info, warning,
	 *                           danger. Default is 'default'.
	 * @param string $size       Optional. Button size class. Accepted values
	 *                           are default, small, xsmall, large. Default is
	 *                           'default'.
	 * @param array    $options  Optional. Associative array of custom options
	 *                           and any standard tag attribute for the tag.
	 *                           Default is empty array.
	 * @param callable $c        Optional. A callback to parse the inner html.
	 * @return string  Button dropdown html.
	 */
	public function buttonDropdown($title, $style='default', $size='default', $options = [], \Closure $c)
	{
		// Open the button dropdown.
		$tag =
		"<button type='button' {classes} data-toggle='dropdown'>\n" .
		"\t{title} <span class='caret'></span>\n" .
		"</button>\n" .
		"<ul class='dropdown-menu' role='menu'>\n";

		// Output the button.
		$output = $this->renderButtonElement($title, $style, $size, $options, $tag);

		// Do any callbacks.
		$output .= $this->runClosure($c);

		// Close the button dropdown.
		$output .= "</ul>\n";

		return $output;
	}

	/*
	 * Notices
	 */

	/**
	 * Creates an 'alert-box' style of notice grid.
	 *
	 * $style can be 'default', 'success', 'info', 'warning', 'danger'
	 *
	 * @param string  $content   Notice contents. Default is success.
	 * @param string  $style     Optional. Button style class. Accepted values
	 *                           are default, primary, success, info, warning,
	 *                           danger. Default is 'default'.
	 * @param boolean $closable  Optional. Toggle the notice close button.
	 * @param array   $options   Optional. Associative array of custom options
	 *                           and any standard element attribute for the tag.
	 * @return string Notice html.
	 */
	public function notice($content, $style='success', $closable=true, $options = [])
	{
		// Parse attributes.
		list($classes, $id, $attributes) = $this->parseStandardOptions($options, 'alert', false);

		// Styles
		switch ($style)
		{
			case 'success':
				$classes .= ' alert-success ';
				break;
			case 'info':
				$classes .= ' alert-info ';
				break;
			case 'warning':
				$classes .= ' alert-warning ';
				break;
			case 'danger':
				$classes .= ' alert-danger ';
				break;
			case 'default':
				$classes .= ' text-muted ';
				break;
		}

		// Open the notice.
		$output = "<div class='{$classes}'>\n";

		$output .= "\t$content\n";

		// Output a close button.
		if ($closable)
		{
			$output .= "\t<a href='#' class='close'>&times;</a>\n";
		}

		// Close the notice.
		$output .= "</div>\n";

		return $output;
	}

	/*
	 * Forms
	 */

	/**
	 * Creates the wrapping code around a form input. Will generate the
	 * label for you, but you will still need to supply the input itself
	 * since those are fairly standard HTML.
	 *
	 * @param string   $label   Input label.
	 * @param array    $options Optional. Associative array of custom options
	 *                          and any standard tag attribute for the tag.
	 * @param callable $c       Optional. A callback to parse the inner html.
	 * @return string Input wrapper html.
	 */
	public function inputWrap($label, $options = [], \Closure $c)
	{
		// Parse attributes.
		list($classes, $id, $attributes) = $this->parseStandardOptions($options, 'form-group', true);

		// Open the input wrap.
		$output = "<div {$id} {$classes} {$attributes}>
		<label for=''>{$label}</label>\n";

		// Do any callbacks.
		$output .= $this->runClosure($c);

		// Close the input wrap.
		$output .= "\t\t</div>\n";

		return $output;
	}

} // BootstrapUIKit.php
