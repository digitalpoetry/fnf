<?php
/**
 * FnF Release Script.
 *
 * Builds a release of application or framework. Usage below.
 *
 * To build a framework release with the application release:
 *     ?fnf
 *
 * @package    DigitalPoetry\FnF
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things 
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

// Include file bootstrap.
require_once 'build' . DIRECTORY_SEPARATOR . 'bootstrap.php';

// Build the Application.
require SCRIPTSPATH . DS . 'build_before.php';
require SCRIPTSPATH . DS . 'build.php';
require SCRIPTSPATH . DS . 'build_after.php';

// Release the Application.
require SCRIPTSPATH . DS . 'release_before.php';
require SCRIPTSPATH . DS . 'release.php';
// Build a framework release?
if (isset($_GET['fnf'])) {
	require SCRIPTSPATH . DS . 'release_fnf.php';
}
require SCRIPTSPATH . DS . 'release_after.php';
require SCRIPTSPATH . DS . 'codex.php';

// Commit to repository.
/** @todo Execute a shell command to push to git repo */
