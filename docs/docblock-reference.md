
Dockblocks Reference
===

**Links:**
- [phpDoc Documentation](https://www.phpdoc.org/docs/latest/index.html)
- [PSR-5: PHPDoc Standard](https://github.com/phpDocumentor/fig-standards/tree/master/proposed)
- [WordPress PHP Documentation Standards](https://make.wordpress.org/core/handbook/best-practices/inline-documentation-standards/php/#resources)
- [PEAR Standards](http://pear.php.net/manual/en/standards.sample.php)
- [PHP Annotations ](http://php-annotations.readthedocs.io/en/latest/ConsumingAnnotations.html)


Tag Elements
---

> Tag             Element           Description
> author          Any               documents the author of the associated element.
> copyright       Any               documents the copyright information for the associated element.
> deprecated      Any               indicates that the associated element is deprecated and can be removed in a future version.
> example         Any               shows the code of a specified example file or, optionally, just a portion of it.
> ignore          Any               tells phpDocumentor that the associated element is not to be included in the documentation.
> internal        Any               denotes that the associated elements is internal to this application or library and hides it by default.
> link            Any               indicates a relation between the associated element and a page of a website.
> see             Any               indicates a reference from the associated element to a website or other elements.
> since           Any               indicates at which version the associated element became available.
> todo            Any               indicates whether any development activity should still be executed on the associated element.
> uses            Any               indicates a reference to (and from) a single associated element.
> version         Any               indicates the current version of Structural Elements.
> source          Any, except File  shows the source code of the associated element.
> filesource      File              includes the source of the current file for use in the output.
> license         File, Class       indicates which license is applicable for the associated element.
> package         File, Class       categorizes the associated element into a logical grouping or subdivision.
> method          Class             allows a class to know which ‘magic’ methods are callable.
> property        Class             allows a class to know which ‘magic’ properties are present.
> property-read   Class             allows a class to know which ‘magic’ properties are present that are read-only.
> property-write  Class             allows a class to know which ‘magic’ properties are present that are write-only.
> api             Methods           declares that elements are suitable for consumption by third parties.
> param           Method, Function  documents a single argument of a function or method.
> return          Method, Function  documents the return value of functions or methods.
> throws          Method, Function  indicates whether the associated element could throw a specific type of exception.
> global          Variable          informs phpDocumentor of a global variable or its usage.
> var             Properties        defines which type of data is represented by the value of a property. MUST contain the name of the element it documents.


Tag Formats
---

> @api                                                                     with a public visibility which are intended to be the public API
> @author [name] [<email address>]                                         if an e-mail address is provided it MUST follow the author’s name and be contained in chevrons, or angle brackets
> @copyright [description]                                                 RECOMMENDED to mention the year(s) which are covered by this copyright and the organization involved
> @deprecated [0.1.0] [<description>]                                  declares that the associated Structural elements will be removed in a future version
> @example [location] [<start-line> [<number-of-lines>] ] [<description>]  !!! used to demonstrate the use of Structural Elements by presenting the contents of files that use them.
> @filesource                                                              FILE DOCBLOCK ONLY tells phpDocumentor to include the current file in the parsing output.
> @global [Type] [name] @global [Type] [description]                       !!! used to inform phpDocumentor of a global variable or its usage
> @ignore [<description>]                                                  RECOMMENDED (but not required) to provide an additional description stating why the associated element is to be ignored.
> @internal [description]                                                  can be used as counterpart of the @api tag, indicating that the associated Structural Elements are used purely for the internal workings of this piece of software.
> @license [<url>] [name]                                                  RECOMMENDED for file-level PHPDocs ONLY, provides the user with the name and URL of the license that is applicable to Structural Elements and any of their child elements.
> @link [URI] [<description>]                                              !!! indicates a custom relation between associated Structural Elements and a website
> @method [return type] [name]([[type] [parameter]<...>]) [<description>]  MUST NOT be used with a class or interface. Used in situation where a class contains the __call() magic method and defines some definite uses,
> @package [level 1]\[level 2]\[etc.]                                      can be used as a counterpart or supplement to Namespaces.
> @param [Type] [name] [<description>]                                     limited to methods or functions ONLY .MUST contain a Type to indicate what is expected
> @property [Type] [name] [<description>]                                  MUST NOT be used with a class or interface. Used in the situation where a class contains the __get() and __set() magic methods
> @property-read [Type] [name] [<description>]                             MUST NOT be used with a class or interface. used in the situation where a class contains the __get() magic method and allows for specific names that are not covered in a __set() magic method.
> @property-write [Type] [name] [<description>]                            MUST NOT be used with a class or interface. used in the situation where a class contains the __set() magic method and allows for specific names that are not covered in a __get() magic method.
> @return [Type] [<description>]                                           MUST contain a Type to indicate what is returned; the description on the other hand is OPTIONAL yet RECOMMENDED in case of complicated return structures, such as associative arrays. The @return tag MAY have a multi-line description and does not need explicit delimiting. It is RECOMMENDED when documenting to use this tag with every function and method.
> @see [URI | FQSEN] [<description>]                                       can be used to define a reference to other Structural Elements or to an URI.
> @since [version] [<description>]                                         !!! can be used to indicate since which version specific Structural Elements have become available. can occur multiple times within a PHPDoc. In that case, each occurrence is treated as an entry to a change log.
> @source [<start-line> [<number-of-lines>] ] [<description>]              !!! used to communicate the implementation of Structural Elements by presenting their source code, or more typically - portions of it.
> @throws [Type] [<description>]                                           MAY be used to indicate that Structural Elements could throw a specific type of error.
> @todo [description]                                                      MUST be accompanied by a description, used to indicate that an activity surrounding the associated Structural Elements must still occur.
> @uses [FQSEN] [<description>]                                            used to describe a consuming relation between the current element and any other of the Structural Elements.
> @used-by [FQSEN] [<description>]                                         Not official, Recommened to be used in conjunction with @uses tags
> @var [“Type”] [$element_name] [<description>]                            defines which type of data is represented by the value of a property. MUST contain the name of the element it documents.
> @version [<vector>] [<description>]                                      used to indicate the current version of Structural Elements.
> @inheritDoc                                                              Not official (coming soon), make clear that an element has been explicitly documented (and thus not forgotten)


Inline Tag Formats
---

> {@example [location] [<start-line> [<number-of-lines>] ] [<description>]}  !!! used to demonstrate the use of Structural Elements by presenting the contents of files that use them.
> {@internal [description]}}                                                 can be used as counterpart of the @api tag, indicating that the associated Structural Elements are used purely for the internal workings of this piece of software.
> {@inheritdoc}                                                              The inline tag {@inheritDoc} is used in a description to import the description of a parent element, even if the child element already has a description.
> {@todo [description]}                                                      MUST be accompanied by a description, used to indicate that an activity surrounding the associated Structural Elements must still occur.
> {@link [URI] [<description>]}                                              !!! indicates a custom relation between associated Structural Elements and a website, Currently with PHPDoc, there’s only URI support (i.e. no support for Structural Elements), and even that is available only in long descriptions.
> {@see [URI | FQSEN] [<description>]}                                       can be used to define a reference to other Structural Elements or to an URI.


Marker Formats
---
> // TODO: [description]   PHPDoc Marker version of @todo tag
> // FIXME: [description]  PHPDoc Marker, Recommended to use todo marker instead


Primatives
---

> string           piece of text of an unspecified length.
> int or integer   whole number that may be either positive or negative.
> float            real, or decimal, number that may be either positive or negative.
> bool or boolean  variable that can only contain the state ‘true’ or ‘false’.
> array            collection of variables of unknown type. It is possible to specify the types of array members, see the chapter on arrays for more information.
> resource         file handler or other system resource as described in the PHP manual.
> null             value contained, or returned, is literally null. This type is not to be confused with void, which is the total absence of a variable or value (usually used with the @return tag).
> callable         function or method that can be passed by a variable, see the PHP manual for more information on callables.


Keywords
---

> mixed          A value with this type can be literally anything; the author of the documentation is unable to predict which type it will be.
> void           This is not the value that you are looking for. The tag associated with this type does not intentionally return anything. Anything returned by the associated element is incidental and not to be relied on.
> object         An object of any class is returned,
> false or true  An explicit boolean value is returned; usually used when a method returns ‘false’ or something of consequence.
> self           An object of the class where this type was used, if inherited it will still represent the class where it was originally defined.
> static         An object of the class where this value was consumed, if inherited it will represent the child class. (see late static binding in the PHP manual).
> $this          This exact object instance, usually used to denote a fluent interface.


----------


About DocBlocks
---

[DocBlocks](https://github.com/phpDocumentor/fig-standards/blob/master/proposed/phpdoc.md#51-summary)

**DocBlocks MUST:**
- start with the character sequence /** followed by a whitespace character
- end with */ and
- have zero or more lines in between.

Single line example:
```
/** @var str $str This is a good comment. */
```

Multiline example:
```
/**
 * <...>
 */
```

Not a example:
```
    // This is not a docblock
```

Foreach statement is not considered to be a "Structural Element" but a Control
Flow statement.
```
/** @var \Sqlite3 $sqlite */
foreach($connections as $sqlite) {

    // there should be no docblock here
    $sqlite->open('/my/database/path');
    <...>
}
```

**Structual Elements:**
- file
- require(_once)
- include(_once)
- class
- interface
- trait
- function
- method
- property
- constant
- variable
- global variable

**The following notations can be used per type of "Structural Element":**
> Namespace:       \My\Space
> Function:        \My\Space\myFunction()
> Constant:        \My\Space\MY_CONSTANT
> Class:           \My\Space\MyClass
> Interface:       \My\Space\MyInterface
> Trait:           \My\Space\MyTrait
> Method:          \My\Space\MyClass::myMethod()
> Property:        \My\Space\MyClass::$my_property
> Class Constant:  \My\Space\MyClass::MY_CONSTANT


**The PHPDoc Format:**
- Summary MUST contain an abstract of the "Structural Element".
- If a Description is provided, then it MUST be preceded by a Summary.
- The Description is OPTIONAL but SHOULD be included when the Summary is inadequate.


The description of a tag MUST support Markdown as a formatting language. Due to the nature of
Markdown it is legal to start the description of the tag on the same or the subsequent line
and interpret it in the same way.

**So the following tags are semantically identical:**
```
/**
 * @var string This is a description.
 * @var string This is a
 *    description.
 * @var string
 *    This is a description.
 */
```


@inheritDoc tag is used to make inheritance explicit. Use the @inheritDoc inline
tag to augment a Description.

**Class / Interface:**
  - MUST inherit @package.
  - Should inherit @subpackage, unless @package clashes at the parent level.

**Function / Method in a Class or Interface MUST inherit:**
  - @param
  - @return
  - @throws

**Constant / property in a Class MUST inherit:**
  - @var:


The `@api` tag is used to declare "Structural Elements" as being suitable for
consumption by third parties.


The `@deprecated` tag is used to indicate which 'Structural elements' are
deprecated and are to be removed


The `@author` tag is used to document the author of any "Structural Element".


The `@copyright` tag is used to document the copyright information of any
"Structural element".


The `@example` tag is used to link to an external source code file which contains
an example of use:
```
@example [URI] [<description>]
```
or inline:
```
    - {@example [URI] [:<start>..<end>]}
```


The `@global` tag is used to denote a global variable or its usage.

**Syntax for the Global's Definition:**
- Only one @global tag MAY be allowed per global variable DocBlock.
- A global variable DocBlock MUST be followed by the global variable's
  definition before any other element or DocBlock occurs.
- The name MUST be the exact name of the global variable as it is declared in
  the source.

**Syntax for the Global's Usage:**
- The function/method @global syntax MAY be used to document usage of global
  variables in a function, and MUST NOT have a $ starting the third word.
- The "Type" will be ignored if a match is made between the declared global
  variable and a variable documented in the project.


The `@internal` tag is used to denote that the associated "Structural Element"
is a structure internal to this application or library. It may also be used
inside a description to insert a piece of text that is only applicable for the
developers of this software.


The `@license` tag is used to indicate which license is applicable for the
associated 'Structural Elements':
```
@license [<SPDX identifier>|URI] [name]
```


The `@link` tag is deprecated in favor of the @see tag, which since this
specification may relate to URIs.


The @method allows a class to know which 'magic' methods are callable. The
@method tag is used in situation where a class contains the __call() magic
method and defines some definite uses. An example of this is a child class whose
parent has a __call() to have dynamic getters or setters for predefined
properties. The child knows which getters and setters need to be present but
relies on the parent class to use the __call() method to provide it. In this
situation, the child class would have a @method tag for each magic setter or
getter method. The @method tag allows the author to communicate the type of the
arguments and return value by including those types in the signature. When the
intended method does not have a return value then the return type MAY be
omitted; in which case 'void' is implied. @method tags MUST NOT be used in a
PHPDoc that is not associated with a class or interface.

Example:
```
class Parent
{
    public function __call()
    {
        <...>
    }
}

/**
 * @method string getString()
 * @method void setInteger(int $integer)
 * @method setString(int $integer)
 */
class Child extends Parent
{
    <...>
}
```


The @package tag is used to categorize "Structural Elements" into logical subdivisions.
    - The @package tag can be used as a counterpart or supplement to Namespaces.
    - SYNTAX: @package [level 1]\[level 2]\[etc.]
    Example:
    ```
        /**
         * @package PSR\Documentation\API
         */
    ```


The @param tag is used to document a single parameter of a function or method:
```
@param ["Type"] [name] [<description>]
```


The @property tag is used in the situation where a class contains the __get()
and __set() magic methods and allows for specific names. An example of this is a
child class whose parent has a __get(). The child knows which properties need to
be present but relies on the parent class to use the __get() method to provide
it. In this situation, the child class would have a @property tag for each magic
property. @property tags MUST NOT be used in a "PHPDoc" that is not associated
with a class or interface.
Example:
```
    class Parent
    {
        public function __get()
        {
            <...>
        }
    }

    /**
     * @property string $myProperty
     */
    class Child extends Parent
    {
        <...>
    }
```


The `@return` tag is used to document the return value of functions or methods.


The @see tag indicates a reference from the associated "Structural Elements"
to a website or other "Structural Elements":
```
@see [URI | "FQSEN"] [<description>]
```


The @since tag is used to denote when an element was introduced or modified:
```
@since [<"Semantic Version">] [<description>]
```


The @throws tag is used to indicate whether "Structural Elements" throw a specific type of exception:
```
@throws ["Type"] [<description>]
```


The @todo tag is used to indicate whether any development activities should still be executed:
```
@todo [description]
```


The @uses Indicates whether the current "Structural Element" consumes the "Structural Element", or project file, that is provided as target:
```
@uses [file | "FQSEN"] [<description>]
```


The @var tag documents the "Type" of the following "Structural Elements":
```
@var ["Type"] [element_name] [<description>]
```


The @version tag is used to denote some description of "versioning" to an
element. The @version tag MAY NOT be used to show the last modified or
introduction version of an element, the @since tag SHOULD be used for that
purpose:
```
@version ["Semantic Version"] [<description>]
```


The @source tag shows the source code of Structural Elements:
```
@source [<start-line> [<number-of-lines>] ] [<description>]
```


The `@filesource` tag is used to tell phpDocumentor to include the source of the
current file in the parsing results The @filesource tag tells phpDocumentor to
include the current file in the parsing output. As this only applies to the
source code of the entirefile MUST this tag be used in the file-level PHPDoc.
Any other location will be ignored.