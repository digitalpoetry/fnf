<?php
/**
 * FnF Debug helper functions
 *
 * Builds a release of the framework.
 *
 * @package    DigitalPoetry\FnF\Build
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things 
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

/**
 * Console Log Wrapper.
 *
 * Dumps a variable to the browser console.
 *
 * @uses Debug::console_log()
 * @param mixed $var The variable to dump.
 */
function c($var) {
	global $app;
	$app->helpers->debug->console_log($var);
}

