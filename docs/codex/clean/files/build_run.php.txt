<?php
/**
 * FnF Run Script.
 *
 * Builds and runs the application on the fly. Runs the application live or
 * compiled. Running live has usable php error line numbers and filepaths.
 * Script Usage below.
 *
 * Run the application live (default):
 *     ?run=live
 * Run the application compiled:
 *     ?run=compiled
 * Enable cache:
 *     ?cache=all
 * Enable cache by type:
 *     ?cache=files,assets,images
 * List source files to build from cache:
 *     ?cache=files&files=file1,file2,file3
 * Purge temporary files:
 *     ?purge=all
 * Purge temporary files by type:
 *     ?purge=cache,live,release
 * Purge temporary files and die:
 *     ?purge=all,die
 * Install project first:
 *     ?install
 *
 * You shouldn't need to purge the cache since cache files are overwritten when
 * caching is off, but it's there if you want it.
 *
 * @package    DigitalPoetry\FnF
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things 
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

// Include file bootstrap.
require_once 'build' . DIRECTORY_SEPARATOR . 'bootstrap.php';

// Install the project?
if (isset($_GET['install'])) {
	require SCRIPTSPATH . DS . 'install_project.php';
}

// Build the Application.
$bld_s = microtime();
require SCRIPTSPATH . DS . 'build_before.php';
require SCRIPTSPATH . DS . 'build.php';
require SCRIPTSPATH . DS . 'build_after.php';
$bld_e = microtime();

// Run the Application.
$run_s = microtime();
require SCRIPTSPATH . DS . 'run.php';
$run_e = microtime();

// Time stats.
global $app;
$log =
"Powered by Framework in a File.\n" .
"Compiled in: " . number_format($bld_e - $bld_s, 4) . " seconds.\n" .
"Executed in: " . number_format($run_e - $run_s, 4) . " seconds.\n";
$app->helpers->debug->console_log($log);

/*
 * Kint Debugger (http://raveren.github.io/kint). Usage below.
 *
 * Dump a var (fancy):
 *     d($app);
 * Dump a var and kill the script:
 *     dd($app);
 * Dump a var (plain):
 *     s($app);
 * Run a trace:
 *     Kint::trace();
 *     d(1);
 */
# $app->helpers->debug->console_log($app);
c($app);

