<?php
/**
 * FnF Build Configuration
 *
 * Configure the build and release settings.
 *
 * @package    DigitalPoetry\FnF\Build
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

/*
 * Fullpath to the project root folder.
 */
$config['build']['project_root'] = dirname(dirname(__FILE__));

/*
 * Filename to write the compiled application to.
 */
$config['build']['script_name'] = 'app.php';

/*
 * Path to where the compiled application will be written. Must be relative from
 * the project root. No trailing slash.
 */
$config['build']['script_path'] = 'temp';

/*
 * Development url. Used during builds to fetch parsed static assets from
 * scripts like a php files.
 */
$config['build']['url'] = 'http://fnf/';

/*
 * Default run mode for the build_run.php script. Options include:
 * - **live**. Runs a precompiled version of the site from tempoary source files
 *   in another directory. The advantage is that you wont have to cross
 *   reference php error line numbers with the compiled source code to determine
 *   what source file to edit.
 * - **compiled**. Builds and executes the application on the fly displaying the
 *   output on the page.
 */
$config['build']['default_run_mode'] = 'live';

/*
 * Default cache type(s) to use during builds. Options include images, assets,
 * files & all.
 */
$config['build']['cache_options'] = [' images'];

/*
 * Default cache files
 *
 * Default list of files to build from from cached resources when file-caching
 * is enabled. This setting can be overridden by the `?cache_files=` query
 * parameter.
 */
$config['build']['default_cache_files']['system'] =
[
	'app_header.php',
	'Config.php',
	'Bootstrap.php',
	'Application.php',
	'Route.php',
	'Output.php',
	'Loader.php',
	'UIKit.php',
	'Debug.php',
	'Controller.php',
	'app_footer.php'
];

/*
 * Files list
 *
 * This list is automatically generated when the application is compiled.
 * However, if you would like to have the source files cantonated in a
 * particular order, you can specify them here and they will be cantonated first
 * in the order that they appear below. The build script with fill in the rest
 * of the list with files it finds scanning the source directories.
 *
 * NOTE: This also specifies the order in which files are included when running
 * the application live.
 */
$config['build']['files'] =
[
	'system' =>
	[
		'config.php',
		'Bootstrap.php',
		'Application.php',
		'Route.php',
		'Output.php',
		'Loader.php',
		'Controller.php',
		'UIKit.php',
		'Debug.php'
	],
	'app' =>
	[
		'Foo.php',
		'Bar.php',
	],
	'qux' =>
	[
		'Qux.php'
	],
	'quux' =>
	[
		'Quux.php'
	]
];

/*
 * List of files to ignore when copying files during build operations.
 */
$config['build']['ignore_files'] =
[
	'.gitkeep',
	'fnf.sublime-project',
	'fnf.sublime-workspace'
];

/*
 * List of folders to ignore when copying folders during build operations.
 */
$config['build']['ignore_folders'] = ['.git'];

/*
 * List of files to leave when deleting files during build operations.
 */
$config['build']['leave_files'] = [/*'.gitkeep'*/];

/*
 * List of folders to leave when deleting folders during build operations.
 */
$config['build']['leave_folders'] = [/*'.git'*/];

/*
 * File Delimeter
 *
 * Delimiter string used to cantonate source files with during builds.
 */
$config['build']['file_delimiter'] = "\n\n\n";

/*
 * The 4 digit permissions to use when creating new directories.
 */
$config['build']['default_folder_perms'] = 0755;

/*
 * Release version number. Used during release builds.
 */
$config['build']['docblock']['version'] = '0.1.0';

/*
 * releasename of the release. Used during release builds.
 */
$config['build']['docblock']['releasename'] = 'Basic Things';

/*
 * GIT commit reference hash of the release. Used during release builds.
 */
$config['build']['docblock']['checksum'] = '';

