<?php
/**
 * FnF GIT Commit Script
 *
 * Execute a shell command to commit to a git repository.
 *
 * @package    DigitalPoetry\FnF\Build
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

// TODO: Use this file or remove it.

// TODO: Execute a shell command to commit to a git repo.

// Commit to the repository.

$phpdoc = 'silence FnF doc build errors';

