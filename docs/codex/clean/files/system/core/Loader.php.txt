<?php
/**
 * Framework in a File.
 *
 * The micro framework in a single file. Quickly develop and deploy scripts and
 * applications.
 *
 * @package    DigitalPoetry\FnF\Core
 * @author     Jesse LaReaux <jlareaux@gmail.com>
 * @copyright  Copyright (c) 2016, DigitalPoetry http://codeallthethings.xyz
 * @license    MIT License http://opensource.org/licenses/MIT
 * @version    0.1.0 Basic Things
 * @since      0.1.0 Basic Things
 * @link       https://gitlab.com/jlareaux/fnf
 * @filesource
 */

/**
 * Loader Core Class.
 *
 * Instantiates classes and stores class objects for easy access.
 *
 * @package  DigitalPoetry\FnF\Core
 * @author   Jesse LaReaux <jlareaux@gmail.com>
 * @since    0.1.0 Basic Things
 */
class Loader
{
	/**
	 * Array to hold overloaded properties.
	 *
	 * @var array
	 */
	private $libraries = array();

	/**
	 * Set an instance of a library.
	 *
	 * @param string $name  $name The name of the class to set.
	 * @param object $value The value of the class.
	 * @return void
	 */
	public function __set($name, $value)
	{
		$this->libraries[strtolower($name)] = $value;
	}

	/**
	 * Returns an instance of a library.
	 *
	 * @param string $name The name of the class to return.
	 * @return object
	 */
	public function __get($name)
	{
		if (array_key_exists(strtolower($name), $this->libraries)) {
			return $this->libraries[strtolower($name)];
		}
	}

	/**
	 * Checks if a library is set.
	 *
	 * @param string $name The name of the class to check.
	 * @return boolean
	 */
	public function __isset($name)
	{
		if ( isset($this->libraries[strtolower($name)])) {
			return true;
		} else {
			return false;
		}
	}

	/**
	 * Unsets a library if set.
	 *
	 * @param string $name The name of the class to unset.
	 * @return boolean
	 */
	public function __unset($name)
	{
		if ( isset($this->libraries[strtolower($name)])) {
			unset($this->libraries[strtolower($name)]);
		}
	}

	/**
	 * Loads a library.
	 *
	 * Instantiates the given class and overloads a class property with an
	 * object of the given class.
	 *
	 * @param string $class The name of the class to load.
	 * @return void
	 */
	public function load($class)
	{
		// Instantiate the class. Overload a property with the object.
		$name = strtolower($class);
		$this->$name = new $class();
	}

} // Loader

